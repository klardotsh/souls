Vagrant.configure("2") do |config|
	if Vagrant.has_plugin? "vagrant-group"
		config.group.groups = {
			"aio" => ["souls_aio"],
			"multi" => [
				"souls_cerberus",
				"souls_harvester",
			],
		}
	elsif ARGV[0] == "up"
		puts "Make your life easier next time! Install vagrant plugin 'vagrant-group' and run 'vagrant group up aio' or 'vagrant group up multi'!"
		puts "Documentation available at: http://www.rubydoc.info/gems/vagrant-group"
		puts
	end

	# The all-in-one case, for when you're running a smaller op all on one VPS
	config.vm.define "souls_aio" do |node|
		node.vm.box = "ogarcia/archlinux-x64"
		node.vm.synced_folder ".", "/vagrant", type: "rsync",
			rsync__exclude: [
				".git/",
				"target",
			]

		node.vm.provider "virtualbox" do |v|
			v.linked_clone = true
			v.memory = 1024
			v.cpus = 2
			v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
		end

		node.vm.network "forwarded_port", guest: 6379, host: 6379

		# The base Vagrant image is a little too spartan to run Ansible, so fix that
		node.vm.provision "shell", inline: "pacman -Sqy --noconfirm python ansible polkit"

		node.vm.provision "ansible_local" do |ansible|
			ansible.playbook = "vm-ansible/aio.yml"
			ansible.galaxy_role_file = "vm-ansible/aio.roles.yml"
		end
	end

	# Beyond this point we assume you're running the multi-node testing setup,
	# and thus, you're gonna need a bigger boat
	# First, we define a few build workers
	(0..1).each do |i|
		box_name = "souls_build#{i}"

		# In group setups, ensure the workers are started before all other nodes
		if Vagrant.has_plugin? "vagrant-group"
			config.group.groups["multi"].unshift(box_name)
		end

		config.vm.define box_name, autostart: false do |node|
			node.vm.box = "ogarcia/archlinux-x64"
			node.vm.synced_folder ".", "/vagrant", disabled: true

			node.vm.provider "virtualbox" do |v|
				v.linked_clone = true
				v.memory = 512
				v.cpus = 2
				v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
			end
		end
	end

	# Our queueing / messaging node
	config.vm.define "souls_cerberus", autostart: false do |node|
		node.vm.box = "ogarcia/archlinux-x64"
		node.vm.synced_folder ".", "/vagrant", disabled: true

		node.vm.provider "virtualbox" do |v|
			v.linked_clone = true
			v.memory = 256
			v.cpus = 1
			v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
		end
	end

	# Broadcaster of new build requests after detecting new PKGBUILDs
	# In all honesty this could, in production, run on the messaging
	# node, but I want to demonstrate the most ridiculously decoupled
	# case
	config.vm.define "souls_harvester", autostart: false do |node|
		node.vm.box = "ogarcia/archlinux-x64"
		node.vm.synced_folder ".", "/vagrant", disabled: true

		node.vm.provider "virtualbox" do |v|
			v.linked_clone = true
			v.memory = 256
			v.cpus = 1
			v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
		end
	end
end
