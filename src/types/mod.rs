extern crate chrono;

#[derive(Serialize, Deserialize, Debug)]
pub struct DispatcherJob {
    pub dispatched: chrono::DateTime<chrono::UTC>,
    pub package: String,
    pub found_path: String,
    pub repo: String,
    pub repo_path: String,
    pub tarball_b64: String,
}
