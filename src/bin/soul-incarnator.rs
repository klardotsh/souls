extern crate base64;
extern crate redis;
extern crate serde_json;
extern crate souls;

use redis::{Client, Commands};
use souls::types::DispatcherJob;

fn main() {
    let rcon: Client = Client::open("redis://localhost:6379").unwrap();

    loop {
        let (_, raw_job): (String, String) = rcon.blpop("backlog", 0).unwrap();
        let mut job: DispatcherJob = serde_json::from_str(&raw_job).unwrap();

        job.tarball_b64 = String::from_utf8(base64::decode(&job.tarball_b64).unwrap()).unwrap();

        let job = job;

        println!("{:?}", job);
    }
}
