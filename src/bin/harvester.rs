extern crate base64;
extern crate chrono;
extern crate notify;
extern crate redis;
extern crate serde;
extern crate serde_json;
extern crate tar;
extern crate toml;

extern crate souls;

#[macro_use]
extern crate serde_derive;

use std::collections::HashMap;
use std::error::Error;
use std::io::Read;
use std::path::PathBuf;
use std::sync::mpsc::channel;
use std::time::Duration;

use notify::{RecommendedWatcher, Watcher, RecursiveMode};
use notify::DebouncedEvent::{Create, Write};
use redis::Commands;
use souls::types::DispatcherJob;

type HarvestMapping = HashMap<PathBuf, String>;
type RepoVec = Vec<Repository>;

#[derive(Serialize, Deserialize, Debug)]
struct Repository {
    name: String,
    harvest: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Config {
    repositories: RepoVec,
}

fn on_new_package(path: PathBuf, mapping: &HarvestMapping) {
    let pkgname = path.file_name().unwrap();
    let repo = mapping.get(path.parent().unwrap()).unwrap();
    let now = chrono::UTC::now();
    let mut tarbuf = Vec::new();

    {
        let mut tarball = tar::Builder::new(&mut tarbuf);
        tarball.append_dir_all("soul", &path).unwrap();
        tarball.finish().unwrap();
    }

    let tarbuf = tarbuf;

    let job = DispatcherJob {
        dispatched: now,
        package: pkgname.to_string_lossy().into_owned(),
        found_path: path.to_string_lossy().into_owned(),
        repo: repo.clone(),
        repo_path: path.parent()
            .unwrap()
            .to_string_lossy()
            .into_owned(),
        tarball_b64: base64::encode(&tarbuf),
    };

    let buf = serde_json::to_string(&job).unwrap();

    let redis_con: redis::Client = redis::Client::open("redis://localhost:6379").unwrap();
    let result: Result<usize, redis::RedisError> = redis_con.rpush("backlog", buf);
}

fn on_updated_package(mut path: &mut PathBuf, mapping: &HarvestMapping) {
    if path.file_name().unwrap() != "PKGBUILD" {
        return;
    }

    path.pop();

    on_new_package(path.to_path_buf(), mapping);
}

fn watch(repos: RepoVec, mapping: HarvestMapping) -> notify::Result<()> {
    let (tx, rx) = channel();
    let mut watcher: RecommendedWatcher = Watcher::new(tx, Duration::from_secs(1)).unwrap();
    let mut fail_count = 0;

    for r in &repos {
        match watcher.watch(&r.harvest, RecursiveMode::Recursive) {
            Err(e) => {
                println!("Error watching {}: {}", r.name, e.cause().unwrap());
                fail_count += 1;
            }
            _ => {}
        }
    }

    if fail_count == repos.len() {
        println!("Failed to watch any source repos, exiting!");
        std::process::exit(255);
    }

    loop {
        match rx.recv() {
            Ok(event) => {
                match event {
                    Create(path) => on_new_package(path, &mapping),
                    Write(mut path) => on_updated_package(&mut path, &mapping),
                    _ => {}
                }
            }
            Err(e) => println!("watch error: {:?}", e),
        }
    }
}

fn main() {
    let mut config_str = String::new();
    let mut config_file = std::fs::File::open("configs/harvester.example.toml").unwrap();
    config_file.read_to_string(&mut config_str).unwrap();

    let decoded_config: Config = toml::from_str(&config_str).unwrap();
    let mut harvest_map = HarvestMapping::new();

    for repo in &decoded_config.repositories {
        harvest_map.insert(PathBuf::from(repo.harvest.clone()), repo.name.clone());
    }

    if let Err(e) = watch(decoded_config.repositories, harvest_map) {
        println!("error: {:?}", e)
    }
}
